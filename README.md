# Course booklet for the BERD course "Make your research reproducible"

Welcome to the course booklet repository! :wave:

To view the booklet, please run

```
quarto preview mybook
```

in your terminal, or

```r
quarto::quarto_preview()
```

in R.
